﻿using System.Collections.Generic;
using System.Linq;

namespace Helpers
{
    public class PagingResponse<T>
    {
        public int Page { get; set; }

        public int PerPage { get; set; }

        public int Pages { get; set; }

        public T[] Data { get; set; }

        public static PagingResponse<T> GetPageResponse(IEnumerable<T> elements, int perPage, int page)
        {
            PagingResponse<T> pagingResponse = new PagingResponse<T>()
            {
                PerPage = perPage,
                Page = page,
                Pages = elements.Count() / perPage,
                Data = elements.Skip((page - 1) * perPage).Take(perPage).ToArray()
            };
            if (elements.Count() % perPage > 0)
            {
                ++pagingResponse.Pages;
            }

            return pagingResponse;
        }
    }
}
