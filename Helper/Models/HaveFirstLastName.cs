﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Helpers
{
    public class HaveFirstLastName
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
